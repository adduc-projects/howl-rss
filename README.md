Unofficial RSS Feeds for Howl
===


Source code for the [Unofficial RSS Feeds for Howl](https://howl-rss.128.io)
website. This is a service that generates RSS feeds for the shows on
Howl.fm.

Installation
---

1. Download and install [Composer](https://www.getcomposer.org).
2. Run `composer install` to install third-party dependencies
3. Copy `config.env.example` to `config.env`, and edit as needed.
4. Restore SQL in `sql/schema.sql`


License
---

Copyright (C) 2016 John S Long

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
